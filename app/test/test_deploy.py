from __future__ import print_function
import os, unittest, tempfile, logging, sys, yaml
from mock import patch

from context import deploy as d

class FunctionsTestCase(unittest.TestCase):

    def setUp(self):
        pass
    def tearDown(self):
        pass

    def load_sample_response(self, file, folder=None):
        # loads sample JSON data from our test library and returns it
        # as a dict. these are used as output from Fusebill API calls
        #
        # @method load_sample_response
        # @public
        #
        # @param {str} file the filename to load
        # @param {str} [folder=None] optional folder from which to load the file

        if folder:
            folder = 'responses/%s' % folder
        else:
            folder = 'responses'

        with open('%s/%s.json' % (folder, file), 'rb') as infile:
            data = json.load(infile)

        return data

    def test_load_config(self):
        # Test that we can load a provided configuration file
        
        path = os.path.dirname(os.path.abspath(__file__))
        file = 'test_config.yml'
        config = yaml.load(open(file))

        r = d.load_config('%s/%s' % (path, file))

        self.assertEqual(config, r)


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(FunctionsTestCase)
    try:
        unittest.TextTestRunner(failfast=True, verbosity=1).run(suite)
    except KeyboardInterrupt:
        print("Exiting after Ctrl-C")
    finally:
        sys.exit(0)


