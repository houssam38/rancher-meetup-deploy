#!/usr/bin/env python

import requests, json, yaml, time, argparse, sys, os

def load_config(path):
    '''
    Loads and returns the provided configuration file
    '''

    return yaml.load(open(path))


def get_environments():
    '''
    Lists all environments visible to this API key
    '''
    url = '%s/projects/' % config['endpoint']

    r = requests.get(url, auth=AUTH)

    if r.ok:
        return r

    return []

def get_environment(name=None, env_id=None):
    '''
    returns the id of an environment with the provided name or `None`
    '''

    r = get_environments()
    r.raise_for_status()
    
    environments = r.json()['data']

    for env in environments:
        if env['name'] == name or env['id'] == env_id:
            return env

    return None

def get_project_members(env_id):
    '''
    Gets the project members for a particular environment
    '''

    url = '%s/projects/%s/projectmembers' % ( config['endpoint'], env_id )

    r = requests.get(url, auth=AUTH)

    if r.ok:
        response = r.json()['data']
        return response

    return []

def _sanitize_response(response):
    '''
    strips out extra API fields
    '''

    for key in ['links', 'actions']:
        try:
            del response[key]
        except KeyError:
            pass

    return response

def _sanitize_members(members):
    '''
    performs additional sanitation of members object
    '''

    for member in members:
        try:
            del member['baseType']
        except KeyError:
            pass
        
        member['projectId'] = None

    return members


def create_environment(name, projectTemplateId, projectMembers=[], **kwargs):
    '''
    Creates an environment with the provided items
    '''

    url = '%s/projects' % config['endpoint']

    projectMembers = _sanitize_members(projectMembers)

    data = dict(name=name, projectTemplateId=projectTemplateId, projectMembers=projectMembers, **kwargs)

    r = requests.post(url, data=json.dumps(data), auth=AUTH)

    return r

def _act_on_environment(env_id, action):
    '''
    Actually calls an action on an environment
    '''

    url = '%s/projects/%s/' % (config['endpoint'], env_id)

    params = dict(action=action)

    r = requests.post(url, params=params, auth=AUTH)

    return r

def deactivate_environment(env_id):
    '''
    Deactivates the provided environment
    '''

    response = _act_on_environment(env_id, 'deactivate')

    return response

def remove_environment(env_id, purge=True):
    '''
    Deletes the provided environment
    '''

    response = _act_on_environment(env_id, 'remove')

    if purge:
        response = purge_environment(env_id)

    return response

def purge_environment(env_id):
    '''
    Deletes the provided environment
    '''

    response = _act_on_environment(env_id, 'purge')

    return response

def get_project_template_id(name):
    '''
    Returns the id for a template with the provided name
    '''

    url = '%s/projecttemplates/' % config['endpoint']

    r = requests.get(url, auth=AUTH)

    if r.ok:
        for env in r.json()['data']:
            if env['name'].lower() == name.lower():
                return env['id']

    return None

def add_host(env_id, host_config, provider='ec2'):
    '''
    adds a host with the provided config in the referenced provider
    '''

    if provider == 'ec2':
        return add_ec2_host(env_id, host_config)

    return dict(status='error', message='provider %s is not supported' % provider)

def add_ec2_host(env_id, host_config):
    '''
    adds an ec2 host
    '''

    url = '%s/projects/%s/host/' % (config['endpoint'], env_id)
    r = requests.post(url, data=json.dumps(host_config), auth=AUTH)

    return r

def wait_for_env_to_exist(env_id, interval=5, max_wait=300):
    '''
    waits for an environment to be registered
    '''

    count = 0
    env = get_environment(env_id=env_id)

    if not env:
        return False

    while env['state'] == 'registering':
        time.sleep(interval)
        count += interval

        if count >= max_wait:
            return False
        
        env = get_environment(env_id=env_id)

    return True

def env_is_stable(env_id):
    '''
    determines if an environment is stable by making sure that all stacks are healthy
    '''

    r = get_stacks(env_id)
    
    if r.ok:
        stacks = r.json()['data']

        for stack in stacks:
            if stack['state'] != 'active' or stack['healthState'] != 'healthy':
                return False

        return True

    r.raise_for_status()

def wait_for_env_to_stabilize(env_id, interval=5, max_wait=300, output_char='.', print_output=False):
    '''
    waits for an environment to stabilize
    '''

    count = 0
    while not env_is_stable(env_id):
        if print_output:
            sys.stdout.write(output_char)

        time.sleep(interval)
        count += interval

        if count >= max_wait:
            return False

    return True

def get_stacks(env_id):
    '''
    returns all stacks for an environment
    '''

    url = '%s/projects/%s/stacks/' % (config['endpoint'], env_id)

    r = requests.get(url, auth=AUTH)

    return r

def get_stack(env_id, name=None, stack_id=None):
    '''
    returns a stack with the provided name or id or `None`
    '''

    stacks = get_stacks(env_id)

    for stack in stacks:
        if stack['name'] == name or stack['id'] == stack_id:
            return stack

    return None

def create_registry(env_id, address):
    '''
    creates a registry entry
    '''

    url = '%s/projects/%s/registry/' % (config['endpoint'], env_id)
    data = dict(type='registry', serverAddress=address)

    r = requests.post(url, data=json.dumps(data), auth=AUTH)

    return r

def set_registry_credentials(env_id, registry_id, username, password):
    '''
    sets credentials on the provided registry in the provided environment
    '''

    url = '%s/projects/%s/registrycredential/' % (config['endpoint'], env_id)
    data = dict(type='registryCredential', 
                email='not-really@required.anymore',
                publicValue=username,
                secretValue=password,
                registryId=registry_id
            )

    r = requests.post(url, data=json.dumps(data), auth=AUTH)

    return r

def create_stack(env_id, name, dockerCompose, startOnCreate=True, **kwargs):
    '''
    creates a stack from the provided configuration
    '''

    url = '%s/projects/%s/stack/' % (config['endpoint'], env_id)
    if 'rancherCompose' in kwargs:
        if type(kwargs['rancherCompose']) == dict:
            rancherCompose = kwargs['rancherCompose']
        else:
            rancherCompose = yaml.load(kwargs['rancherCompose'])

        del kwargs['rancherCompose']
    else:   
        rancherCompose = {
            'version': '2',
            'services': {}
        }

        for service in dockerCompose['services'].keys():
            rancherCompose['services'][service] = dict(scale=1, start_on_create=True)

    data = dict(name=name, 
                dockerCompose=yaml.dump(dockerCompose, default_flow_style=False), 
                rancherCompose=yaml.dump(rancherCompose, default_flow_style=False), 
                type="stack", startOnCreate=startOnCreate, **kwargs)

    r = requests.post(url, data=json.dumps(data), auth=AUTH)

    return r

def get_services(env_id, stack_id, filter=None):
    '''
    returns services for the provided stack in the provided environment
    '''

    url = '%s/projects/%s/stack/%s/services' % (config['endpoint'], env_id, stack_id)
    if filter:
        # this doesn't actually work - the API doesn't filter
        r = requests.get(url, auth=AUTH, params=filter)
    else:    
        r = requests.get(url, auth=AUTH)

    return r

def get_service(env_id, stack_id, name=None, service_id=None):
    '''
    get a service by ID or by Name
    '''

    r = get_services(env_id, stack_id)

    r.raise_for_status()

    services = r.json()['data']

    for service in services:
        if service['name'] == name or service['id'] == service_id:
            return service

    return None

def upgrade_service(env_id, stack_id, service_id, launchConfig=None,     complete=True, batchSize=1, intervalMillis=2000, secondaryLaunchConfigs=[], startFirst=False):
    '''
    upgrades the provided service
    '''

    # TODO: add launch config support

    url = '%s/projects/%s/services/%s' % (config['endpoint'], env_id, service_id)
    data = {}
    params = dict(action='upgrade')

    strategy = dict(batchSize=batchSize, intervalMillis=intervalMillis, secondaryLaunchConfigs=secondaryLaunchConfigs, startFirst=startFirst)

    if launchConfig:
        strategy['launchConfig'] = launchConfig
    else:
        service = get_service(env_id, stack_id, service_id=service_id)
        if service:
            strategy['launchConfig'] = service['launchConfig']

    data['inServiceStrategy'] = strategy
    data['toServiceStrategy'] = 'null'

    r = requests.post(url, auth=AUTH, params=params, data=json.dumps(data))

    if r.ok and complete:
        return finish_upgrade(env_id, stack_id, service_id)

    return r

def finish_upgrade(env_id, stack_id, service_id, action='finishupgrade', sleep_interval=2, max_wait=300):
    '''
    completes an upgrade
    '''

    # set up a sleep counter
    count = 0

    url = '%s/projects/%s/services/%s' % (config['endpoint'], env_id, service_id)
    data = {}
    params = dict(action=action)

    # make sure we're in the right state
    service = get_service(env_id, stack_id, service_id=service_id)

    if service['state'].lower() not in ['upgrading', 'upgraded' ]:
        # TODO figure out a proper response to return
        print('Invalid state %s' % service['state'])
        return False

    while service['state'] == 'upgrading':
        time.sleep(sleep_interval)
        count += sleep_interval
        if count >= max_wait:
            break

        service = get_service(env_id, stack_id, service_id=service_id)

    r = requests.post(url, auth=AUTH, params=params)

    return r

def rollback(env_id, stack_id, service_id):
    '''
    rolls back an upgrade
    '''

    return finish_upgrade(env_id, stack_id, service_id, action='rollback')

def get_endpoints(env_id, stack_id, service_id):
    '''
    return a list of public `ip:port` combinations for a service
    '''

    endpoints = []

    service = get_service(env_id, stack_id, service_id=service_id)

    if 'publicEndpoints' in service:
        for endpoint in service['publicEndpoints']:
            if endpoint['port'] == 80:
                protocol = 'http://'
            elif endpoint['port'] == 443:
                protocol = 'https://'
            else:
                protocol = None

            if protocol:
                endpoints.append('%s%s/' % (protocol, endpoint['ipAddress']))
            else:
                endpoints.append('%s:%d' % (endpoint['ipAddress'], endpoint['port']))

    return endpoints

def deploy():

    # get our template and members
    template_id = get_project_template_id(config['environment']['template'])
    members = get_project_members(config['environment']['default'])

    # create the environment
    env_name = '%s-%s' % (config['environment']['name'], os.environ['CI_COMMIT_SHA'][:8])
    r = create_environment(env_name, template_id, members)

    r.raise_for_status()

    env = r.json()
    env_id = env['id']

    r = wait_for_env_to_exist(env_id)

    if not r:
        print("Timed out waiting for environment to exist.")
        sys.exit(1)

    print("Created environment: %s (%s)" % (env_name, env_id))

    ## create a host
    host_config = config['environment']['host']['config']
    host_config['hostname'] = '%s-%s' % (config['environment']['name'], os.environ['CI_COMMIT_SHA'][:8])
    host_config['amazonec2Config']['accessKey'] = os.environ['AWS_ACCESS_KEY']
    host_config['amazonec2Config']['secretKey'] = os.environ['AWS_SECRET_KEY']

    r = add_host(env_id, host_config)

    host = r.json()
    host_id = host['id']

    print("Created host: %s" % host_config['hostname'])

    # wait for things to settle
    sys.stdout.write("Waiting for host to come online...")
    r = wait_for_env_to_stabilize(env_id, interval=10, print_output=True)

    if not r:
        print('error.')
        print("Timed out waiting for environment to stabilize.")
        sys.exit(1)
    else:
        print('done.')

    # create the registry object
    r = create_registry(env_id, os.environ['CI_REGISTRY'])
    r.raise_for_status()

    registry = r.json()

    # add credentials
    r = set_registry_credentials(env_id, registry['id'], os.environ['CI_REGISTRY_USER'], os.environ['CI_JOB_TOKEN'])

    r.raise_for_status()

    print("Added registry: %s" % os.environ['CI_REGISTRY'])

    # deploy stack and service

    # create our base docker compose file
    d_compose = {
        'version': '2',
        'services': {
            config['service']['name']: {
                'image': '%s/%s:%s' % (os.environ['CI_REGISTRY'], os.environ['CI_PROJECT_PATH'], os.environ['CI_COMMIT_REF_NAME'])
            }
        }
    }

    # add additional key/value pairs
    if 'docker-compose' in config['service']:
        for key, value in config['service']['docker-compose'].items():
            d_compose['services'][config['service']['name']][key] = value

    # actually create the object
    r = create_stack(env_id, config['stack']['name'], d_compose)

    r.raise_for_status()

    stack = r.json()
    stack_id = stack['id']

    print("Created stack: %s (%s)" % (stack['name'], stack_id))

    sys.stdout.write('Waiting for stack to come online...')
    r = wait_for_env_to_stabilize(env_id, max_wait=60, print_output=True)

    if not r:
        print('error.')
        print("Timed out waiting for environment to stabilize.")
        sys.exit(1)
    else:
        print('done.')

    service = get_service(env_id, stack_id, name=config['service']['name'])

    # TODO: test service
    service_id = service['id']

    r = get_endpoints(env_id, stack_id, service_id)

    print('The application is visible at:')
    for endpoint in r:
        print('  - %s' % endpoint)

def teardown(interval=5, max_wait=60):
    env_name = '%s-%s' % (config['environment']['name'], os.environ['CI_COMMIT_SHA'][:8])

    env = get_environment(name=env_name)

    if env:
        env_id = env['id']

        r = deactivate_environment(env_id)

        r.raise_for_status()

        # fetch the state of the environment
        env = get_environment(name=env_name)
        count = 0
        while env['state'].lower() != 'inactive':
            time.sleep(interval)
            count += interval

            if count >= max_wait:
                print('Timed out waiting for environment to deactivate.')
                return False

            # fetch the status again
            env = get_environment(name=env_name)
        
        r = remove_environment(env_id)

        r.raise_for_status()

        print("Removed the environment: %s (%s)" % (env_name, env_id))

    else:
        print("Environment %s doesn't exist." % env_name)



def usage():
    print('Usage: %s {deploy,teardown}' % sys.argv[0])
    sys.exit(1)

def main():
    try:
        action = sys.argv[1]
    except IndexError:
        usage()

    global config, AUTH

    config = load_config('config.yml')
    AUTH = (os.environ['RANCHER_ACCESS_KEY'], os.environ['RANCHER_SECRET_KEY'])

    # set our output not to buffer
    sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)

    if action == 'deploy':
        try:
            deploy()
        except:
            # teardown()
            raise            
    elif action == 'teardown':
        teardown()
    else:
        print("Unknown action %s" % action)
        usage()

if __name__ == '__main__':     
    main()

